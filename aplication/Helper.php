<?php
/**
  * Clase Helper
  * 
  *  Clase Helper sirve para almacenar directorio del system
  * @author Edwin Omar Poot Díaz<edwin.poot.diaz@gmail.com> 
  * @copyright  2016 
  * @return object
  */
class Helper{}

class Html extends Helper
{
		/**
		 * Almacena un directorio especifico.
		 * @param type $title 
		 * @param type|null $url obtiene el directorio.
		 * @return object
		 */
		public function link($title, $url = null){
			if (is_array($url)) {
				if (!empty($url["controller"]) and !empty($url["method"])) {
					$url = "/".implode("/", $url);
				}else if(!empty($url["controller"])){
					$url = "/".$url["controller"];
					if (!empty($url["method"])) {
						$url .= "/".$url["method"];
					}
				}else if(!empty($url["method"])){
						$url = "/".$url["method"];
				}
			}
			$link = '<a  class="btn btn-success" href = "'.APP_URL.$url.'">'.$title.'</a>';
			return $link;
		}

		/**
		 * Almacena un directorio especifico.
		 * @param type $title 
		 * @param type|null $url obtiene el directorio.
		 * @return object
		 */
		public function linkDelete($title, $url = null){
			if (is_array($url)) {
				if (!empty($url["controller"]) and !empty($url["method"])) {
					$url = "/".implode("/", $url);
				}else if(!empty($url["controller"])){
					$url = "/".$url["controller"];
					if (!empty($url["method"])) {
						$url .= "/".$url["method"];
					}
				}else if(!empty($url["method"])){
						$url = "/".$url["method"];
				}
			}
			$link = '<a  class="btn btn-danger delete" href = "'.APP_URL.$url.'">'.$title.'</a>';
			return $link;
		}
}