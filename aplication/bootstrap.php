<?php
/**
  * Clase usersController
  * 
  *  Métodos sirve para procesar la vista.
  * @author Edwin Omar Poot Díaz<edwin.poot.diaz@gmail.com> 
  * @copyright  2016 
  * @return object
  */
class Bootstrap 
{
	
	function __construct()
	{
		# code...
	}
//no necesita un objeta para llamar la funcion
	//function :: instancia;
	/**
	 * Procesa la vista solicitada.
	 * @param Request $p 
	 * @return void
	 */
	public static function run(Request $p){
		$controllerName = $p->getController()."controller";
		//Ruta del controlador
		$routeController = ROOT."controllers".DS.$controllerName.".php";
		$method = $p->getMethod();
		$args = $p->getArgs();


		if (is_readable($routeController)) {
			include_once($routeController);
			$controller = new $controllerName; 

			if (is_callable(array($controller, $method))) {
				$method = $p->getMethod();
			}else{
				$method = "index";
			}
			//Esta condición sirve para que el usuario no pueda acceder al sistema con la url
			if ($method == "login") {
				
			}else{
				Authorization::logged();
			}

			if (!empty($args)) {
				call_user_func_array(array($controller, $method), $args);
				
			}else{
				call_user_func(array($controller, $method));
			}
		

		}else{
			throw new Exception("Controler no encontrado");
			
		}

	}




}