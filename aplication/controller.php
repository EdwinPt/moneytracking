<?php
/**
  * Clase abstracta AppsController
  * 
  *  Métodos que sirve redireccionar y mandar valores a la vista de los controladores.
  * @author Edwin Omar Poot Díaz<edwin.poot.diaz@gmail.com> 
  * @copyright  2016 
  * @return object
  */

abstract class AppsController
{
	//Se crea un objeto 
	protected $_view;

	abstract public function index(); 

	public function __construct()
	{
		$this->_view = new View(new Request);
		$controller = new Request();
		$className = $controller->getController();
		$this->$className = new classPDO();


	}
	/**
	 * Redireccionamiento dependiendo de la accion solicitada.
	 * @param type|array $url 
	 * @return void
	 */
	public function redirect($url = array()){
		$path = "";
		if ($url["controller"]) {
			$path .=$url["controller"];
		}
		if ($url["method"]) {
			$path .= "/". $url["method"];
		}

		header("LOCATION:" .APP_URL."/".$path);
	}
	/**
	 * Los valores enviados en la vista de cada controlador.
	 * @param type|array $one nombre de array de los valores de la funtion set
	 * @param type|null $two Los valores que van dentro del array de la function set
	 * @return void
	 */
	public function set($one, $two=null){
		if (is_array($one)) {
			if (is_array($two)) {
				$data = array_combine($one, $two);
			}else{
				$data = $one;
			}
		}else{
			$data = array($one=>$two);
		}

		$this->_view->setVars($data);

	}

}