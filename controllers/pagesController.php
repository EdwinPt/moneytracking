<?php
/**
  * Clase pagesController
  * 
  * @author Edwin Omar Poot Díaz<edwin.poot.diaz@gmail.com>
  * @copyright  2016
  * @return void
  */
class PagesController extends AppsController
{
	


	public function __construct()
	{
		parent::__construct();
	}
	
	public function index(){
		$this->_view->setlayout("website");
	}

	public function edit(){
		
	}
}