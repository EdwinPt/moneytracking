<?php

/**
  * Clase usersController
  * 
  *  Métodos que sirve para hacer diferentes peticiones como: agregar, editar y eliminar.
  * @author Edwin Omar Poot Díaz<edwin.poot.diaz@gmail.com>
  * @copyright  2016
  * @return object
  */
class typesController extends AppsController
{
	function __construct()
	{
		parent::__construct();
		$this->_view->setlayout("website");
	}
	/**
	 * Función para enlistar todos los tipos de usarios.
	 * @return void
	 */
	public function index(){
		$types = $this->types->find("types", "all");
		$typesCount = $this->types->find("types", "count");
		$this->set("types", $types);
		$this->set("usersCount", $typesCount);
	}

	/**
	 * Función para agregar tipos de usuarios.
	 * @return void
	 */
	public function add(){
		if ($_SESSION["type_name"]=="Administradores") {

			if ($_POST) {
			//Manda los valores en la funcion save para guardar los registros
			if ($this->types->save("types",$_POST )) {
				$this->redirect(array("controller"=>"types"));
			}else{
				$this->redirect(array("controller"=>"types", "methos"=>"add"));
			}
		}
		$this->set("types", $this->types->find("types"));//Hacemos referencia a la tabla types
		$this->_view->setView("add");//Es una funcion indicamos que vista queremos visualizar
		}else{
			$this->redirect(array("controller"=>"types"));
		}
	}

	/**
	 * Función para editar los tipos de usuarios.
	 * @param $id identificador unico. 
	 * @return void
	 */
	public function edit($id){

		if ($_SESSION["type_name"]=="Administradores") {
			//$this->_view->setlayout("website");
			if ($_GET) {
				if ($id){
					$options = array("conditions"=>"id=".$id);
					$type = $this->types->find("types", "first", $options);
					$this->set("type", $type);
				}else{
					//Redirecciona cuando se hace la peticion de update
					$this->redirect(array("controller"=>"types"));
				}
				//Comporbar si esta recibiendo los datos con el $_POST
				if ($_POST) {
					//Primero le mandamo el nombre d ela tabla y luego el POST es donde estan almacenados los datos a editar
					if ($this->types->update("types", $_POST)) {
						$this->redirect(array("controller"=>"types"));
					}else{
					$this->redirect(array("controller"=>"types", "method"=>"edit/".$_POST["id"]));
					}
					
				}	
			}
		}else{
			$this->redirect(array("controller"=>"types"));
		}	
	}

	/**
	 * Función para elimnar los tipos de usuarios.
	 * @param $id identificador unico.
	 * @return void
	 */
	public function delete($id){
		if ($_SESSION["type_name"]=="Administradores") {
			$conditions = "id=".$id;
			if ($_GET) {
				if ($this->types->delete("types", $conditions)) {
					$this->redirect(array("controller"=>"types"));
				}else{
					$this->redirect(array("controller"=>"types", "method"=>"add"));
				}
			}
		}else{
			$this->redirect(array("controller"=>"types"));
		}
	}			
}