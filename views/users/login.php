<div class="container"><!-- Inicio de container -->
	
	<div class="row">     
            <div class="col-sm-6 col-sm-offset-3 form-box">
            	<div class="form-top">
            		<div class="form-top-left">
            			<h3>Login to our site</h3>
                		<p>Enter your username and password</p>
            		</div>
            		
                </div>
                <div class="form-bottom">
                    <form role="form" action="<?php echo APP_URL."/users/login";?>" method="post" class="login-form">
                    	<div class="form-group">
                    		<label class="sr-only" for="form-username">Username</label>
                        	<input type="text" name="username" placeholder="Username..." class="form-username form-control username-password " id="form-username">
                        </div>
                        <div class="form-group">
                        	<label class="sr-only" for="form-password">Password</label>
                        	<input type="password" name="password" placeholder="Password..." class="form-password form-control username-password " id="form-password">
                        </div>
                        <button type="submit" class="btn btn-login" value="Entrar">Sign in!</button>
                         <label class="checkbox pull-left" style="color: #fff;">
	                    	<input type="checkbox" value="remember-me" style="margin-top: 10px; font-size: 12px" >
		                    Remember me
		                </label>
		                <a href="#" class="pull-right new-account" data-toggle="modal" data-target="#ModalCreateAccount" style="margin-top: 10px; color: #fff;" >Create an account</a><span class="clearfix"></span>


                    </form>
                </div>
            </div>           
	</div><!-- Fin de la clase row -->

        <!-- Modal -->
        <div class="modal fade" id="ModalCreateAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Registration form</h4>
              </div>
              <div class="modal-body">

                <form action="#" method="POST">
                    <div class="form-group">
                        <label for="name" style="font-weight: bold;">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="">
                    </div>

                    <div class="form-group">
                        <label for="username" style="font-weight: bold;">Lastname:</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="">
                    </div>
                    <div class="form-group">
                        <label for="email" style="font-weight: bold;">Email address:</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="password" style="font-weight: bold;">Password:</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
            
                    <button type="submit" class="btn btn-raised btn-primary">SEND</button>
                </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div><!-- Final modal -->
<!-- 
	<h3>Inicio de sesion</h3>

	<form action="<?php echo APP_URL."/users/login";?>" method="POST">

		<p>Username : <input type="text" name="username"></p>
		<p>Password : <input type="password" name="password"></p>
		<p><input type="submit" name="Enviar" value="Entrar"></p>
		
	</form> -->

</div><!-- Fin de container -->


